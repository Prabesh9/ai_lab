male(dashrath).
male(ram).
male(laxman).
male(bharat).
male(luv).
male(kush).
male(angada).

female(kaushalya).
female(sita).
female(urmila).    
female(daughter_of_dashrath).

parent_of(dashrath,ram).
parent_of(kaushalya,ram).
parent_of(dashrath,laxman).
parent_of(dashrath,bharat).
parent_of(ram,luv).
parent_of(sita,luv).
parent_of(ram,kush).
parent_of(sita,kush).
parent_of(laxman,angada).
parent_of(urmila, angada).
parent_of(dashrath,daughter_of_dashrath).

/*Rules*/
father_of(X,Y):- male(X),
    parent_of(X,Y).

mother_of(X,Y):- female(X),
    parent_of(X,Y).

husband_of(X, Y):- male(X),
        parent_of(X,Z),
        parent_of(Y,Z),
        female(Y).

grandfather_of(X,Y):- male(X),
    parent_of(X,Z),
    parent_of(Z,Y).

grandmother_of(X,Y):- female(X),
    parent_of(X,Z),
    parent_of(Z,Y).

sister_of(X,Y):- %(X,Y or Y,X)%
    female(X),
    father_of(F, Y), father_of(F,X),X \= Y.

sister_of(X,Y):- female(X),
    mother_of(M, Y), mother_of(M,X),X \= Y.

aunt_of(X,Y):- female(X),
    parent_of(Z,Y), sister_of(Z,X),!.

brother_of(X,Y):- %(X,Y or Y,X)%
    male(X),
    father_of(F, Y), father_of(F,X),X \= Y.

brother_of(X,Y):- male(X),
    mother_of(M, Y), mother_of(M,X),X \= Y.

uncle_of(X,Y):-
    parent_of(Z,Y), brother_of(Z,X).

ancestor_of(X,Y):- parent_of(X,Y).
ancestor_of(X,Y):- parent_of(X,Z),
    ancestor_of(Z,Y).