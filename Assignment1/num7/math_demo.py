from math import floor, ceil, fabs, factorial

a = 7
b = -3
print(f"Floor:: {floor(a/b)}")
print(f"Ceiling:: {ceil(a/b)}")
print(f"Absolute:: {fabs(a/b)}")
