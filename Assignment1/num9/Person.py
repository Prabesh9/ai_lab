class Person:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def print_name(self):
        print(f"Name:: {self.first_name} {self.last_name}")
        print("Age:: "+self.age)


class PersonDemo:
    if __name__ == '__main__':
        first_name = input("Enter First Name:: ")
        last_name = input("Enter Last Name:: ")
        age = input("Enter age:: ")
        person = Person(first_name, last_name, age)
        person.print_name()
