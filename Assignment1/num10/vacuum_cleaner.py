import random


class VacuumCleanerWorld:
    def __init__(self):
        self.cleanStatus = {
            "A": random.choice([True, False]),
            "B": random.choice([True, False]),
        }
        self.room = random.choice(["A", "B"])
        self.score = 0
        self.status()
        self.cleaned_room = []
        print(f"Starting from room {self.room}\n")

    def move(self):
        print(f"Moving inside room {self.room}")
        self.clean()

    def clean(self):
        if not self.cleanStatus[self.room]:
            print(f"Cleaned room {self.room}")
            self.cleanStatus[self.room] = True
            self.score += 1
        self.cleaned_room.append(self.room)
        self.move_to()

    def move_to(self):
        self.room = "A" if self.room == "B" else "B"
        if self.room in self.cleaned_room:
            print("\nBoth rooms are cleaned")
            self.status()
            print(f"Performance Measure:: {self.score}")
            return ""
        print(f"Moving to room {self.room}")
        self.score -= 1
        self.move()

    def status(self):
        print(f"Status::\n{self.cleanStatus}")


class VacuumWorldDemo:
    if __name__ == '__main__':
        vacuum = VacuumCleanerWorld()
        vacuum.move()
