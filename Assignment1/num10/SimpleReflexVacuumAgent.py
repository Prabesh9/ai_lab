import random


class Environment:
    def __init__(self):
        # instantiate locations and conditions
        # 0 indicates Clean and 1 indicates Dirty
        self.location_condition = {'A': random.randint(0, 1), 'B': random.randint(0, 1)}


class SimpleReflexVacuumAgent(Environment):
    def __init__(self, environment):
        super().__init__()
        print(environment.location_condition)
        # Instantiate performance measurement
        score = 0
        # place vacuum at random location
        vacuum_location = random.randint(0, 1)
        # if vacuum at A
        if vacuum_location == 0:
            print("Vacuum is randomly placed at Location A.")
            # and Location A is Dirty.
            if environment.location_condition['A'] == 1:
                print("Location A is Dirty.")
                # suck the dirt  and mark it clean
                environment.location_condition['A'] = 0
                score += 1
                print("Location A has been Cleaned.")
                # move to B
                print("Moving to Location B...")
                score -= 1
                # if B is Dirty
                if environment.location_condition['B'] == 1:
                    print("Location B is Dirty.")
                    # suck and mark clean
                    environment.location_condition['B'] = 0
                    score += 1
                    print("Location B has been Cleaned.")
            else:
                # move to B
                score -= 1
                print("Moving to Location B...")
                # if B is Dirty
                if environment.location_condition['B'] == 1:
                    print("Location B is Dirty.")
                    # suck and mark clean
                    environment.location_condition['B'] = 0
                    score += 1
                    print("Location B has been Cleaned.")

        elif vacuum_location == 1:
            print("Vacuum randomly placed at Location B.")
            # and B is Dirty
            if environment.location_condition['B'] == 1:
                print("Location B is Dirty.")
                # suck and mark clean
                environment.location_condition['B'] = 0
                score += 1
                print("Location B has been Cleaned.")
                # move to A
                score -= 1
                print("Moving to Location A...")
                # if A is Dirty
                if environment.location_condition['A'] == 1:
                    print("Location A is Dirty.")
                    # suck and mark clean
                    environment.location_condition['A'] = 0
                    score += 1
                    print("Location A has been Cleaned.")
            else:
                # move to A
                print("Moving to Location A...")
                score -= 1
                # if A is Dirty
                if environment.location_condition['A'] == 1:
                    print("Location A is Dirty.")
                    # suck and mark clean
                    environment.location_condition['A'] = 0
                    score += 1
                    print("Location A has been Cleaned.")
        # done cleaning
        print(environment.location_condition)
        print("Performance Measurement: " + str(score))


the_environment = Environment()
theVacuum = SimpleReflexVacuumAgent(the_environment)
