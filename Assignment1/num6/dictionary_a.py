dictionary = {
    "Apple": 50,
    "Banana": 20,
    "Mango": 30,
    "Pineapple": 40,
    "Guava": 60,
    "Watermelon": 35,
    "Grapes": 25,
    "Strawberries": 45,
    "Orange": 65,
    "Papaya": 55,
}
print(dictionary)
print(f"List of fruits:: {dictionary.keys()}")
print(f"Price of apple:: Rs.{dictionary['Apple']} ")
print(f"Price of mango:: Rs.{dictionary['Mango']} ")
print(f"Total Price:: Rs.{sum(dictionary.values())} ")

