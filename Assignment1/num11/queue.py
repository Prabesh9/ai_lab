class QueueDemo:
    queue = list()

    def append(self, num):
        self.queue.append(num)
        self.display_queue()

    def pop(self):
        print(self.queue.pop(0))
        self.display_queue()

    def display_queue(self):
        print(self.queue)


stack = QueueDemo()
stack.append(1)
stack.append(3)
stack.append(4)
stack.pop()
stack.pop()
