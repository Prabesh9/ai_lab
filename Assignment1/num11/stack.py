class StackDemo:
    stack_list = list()

    def append(self, num):
        self.stack_list.append(num)
        self.display_item()

    def pop(self):
        self.stack_list.pop()
        self.display_item()

    def display_item(self):
        print(self.stack_list)


stack = StackDemo()
stack.append(1)
stack.append(3)
stack.append(4)
stack.pop()
stack.pop()
