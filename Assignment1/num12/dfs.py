graph1 = {
    'A': ['B', 'S'],
    'B': ['A'],
    'C': ['D', 'E', 'F', 'S'],
    'D': ['C'],
    'E': ['C'],
    'F': ['C'],
    'S': ['A', 'C']
}

visited = []


def dfs(graph, node):
    global visited
    if node not in visited:
        visited.append(node)
        for n in graph[node]:
            dfs(graph, n)


dfs(graph1, "A")
print(visited)

# graph = {}
# root_node = input("Enter root node")
# visited = []
# queue = [root_node]
# while queue:
#     cur_node = queue[0]
#     if cur_node in visited:
#         queue.pop(0)
#         continue
#     lst = input(f"Enter children of {cur_node}").split(" ")
#     for node in lst:
#         queue.append(node)
#     visited.append(queue.pop(0))
#     graph[cur_node] = lst
#
# for node, children in graph.items():
#     print(f"{node}:: {children}")
