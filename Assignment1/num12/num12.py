def menu():
    print("Select the action:: ")
    print("1) Add node:: ")
    print("2) Add vertex:: ")
    print("3) Display the graph:: ")
    print("4) Exit:: ")
    return input()


class DFS:
    def __init__(self):
        self.graph = dict()
        self.visited = []

    def add_node(self):
        node = input("Enter node to be added:: ")
        self.graph[node] = []

    def add_vertex(self):
        if len(self.graph.keys()) == 0:
            print("No nodes in graph, enter node:: ")
            self.add_node()
        cur_node = input(f"Select a node for adding a vertex{self.graph.keys()}:: ")
        vertex = input("Enter a vertex to be added:: ")
        if vertex not in self.graph[cur_node]:
            self.graph[cur_node].append(vertex)
            if vertex not in self.graph.keys():
                self.graph[vertex] = []
            self.graph[vertex].append(cur_node)
        else:
            print("Vertex already exists!!")

    def display(self, node):
        if node not in self.visited:
            self.visited.append(node)
            for n in self.graph[node]:
                self.display(n)

    def display_graph(self):
        print(self.graph)
        node = input("Enter starting node:: ")
        self.display(node)
        print(self.visited)


class DFSDemo:
    if __name__ == '__main__':
        obj = DFS()
        while True:
            opt = menu()
            if opt == "1":
                obj.add_node()
            elif opt == "2":
                obj.add_vertex()
            elif opt == "3":
                obj.display_graph()
            elif opt == "4":
                break
            else:
                print("Invalid action code!!")
