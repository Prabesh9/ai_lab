class Animal:
    def __init__(self, name):
        self.name = name

    def display(self):
        print(f"Animal's name is {self.name}")


class Dog(Animal):
    def display(self):
        print(f"Dog's name is {self.name}")


class Cat(Animal):
    def display(self):
        print(f"Cat's name is {self.name}")


class ObjectDemo:
    if __name__ == '__main__':
        animal_list = list()
        animal_list.append(Dog("Spike"))
        animal_list.append(Cat("Tom"))
        animal_list.append(Animal("Jerry"))
        for animal in animal_list:
            animal.display()